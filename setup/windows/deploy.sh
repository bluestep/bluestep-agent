#!/usr/bin/env bash

DIR=$(dirname ${0})

set -e

cd ${DIR}

trap 'finish' EXIT

finish() {
  [ -d "${BUILD_ABS_DIR}" ] && rm -rf "${BUILD_ABS_DIR}"
}

if [ -z "${AGENT_UPLOADER_USERNAME}" ]; then
  eval "$(fetch-agent-creds.sh)"
fi

../../src/operator/dist.sh
../../src/win/dist.sh

#because we are using inno docker we need everthing in this directory
BUILD_DIR=../../build/win-setup
BUILD_FILES="${BUILD_DIR}/files"
DIST_DIR=../../dist/win-setup
DIST_EXE="${DIST_DIR}/agent-setup.exe"

rm -rf "${BUILD_DIR}" 2>/dev/null || true
mkdir -p "${BUILD_FILES}"
BUILD_ABS_DIR=$(readlink -f "${BUILD_DIR}")
mkdir -p "${DIST_DIR}"
cp -a ../../dist/operator/* "${BUILD_FILES}"
cp -a ../../dist/win/* "${BUILD_FILES}"
cp ../../resources/login.json "${BUILD_FILES}"/login.json
cp -a resources/* "${BUILD_DIR}"

docker run --rm -i -v "${BUILD_ABS_DIR}":/work amake/innosetup ./agent-setup.iss

echo "Signing agent-setup-unsigned.exe"
fetch-check.sh
#copy cert to a tempfile from fetch-code-cert.sh
CERT_FILE="${BUILD_DIR}/cert"
fetch-code-cert.sh >"${CERT_FILE}"
KEY_FILE="${BUILD_DIR}/key"
fetch-code-key.sh >"${KEY_FILE}"

rm -f "${DIST_EXE}" || true

osslsigncode sign -certs "${CERT_FILE}" -key "${KEY_FILE}" -n "Bluestep Agent" -i "https://www.bluestep.net" -t "http://timestamp.comodoca.com" -in "${BUILD_DIR}/agent-setup-unsigned.exe" -out "${DIST_EXE}"

rm -f "${CERT_FILE}" "${KEY_FILE}"

echo "Build Complete: ${DIST_EXE}"

echo "Uploading agent-setup.exe"
curl -X PUT -u "${AGENT_UPLOADER_USERNAME}:${AGENT_UPLOADER_PASSWORD}" \
  --upload-file "${DIST_EXE}" \
  "https://agent.bluestep.net/files/1475676/agent-setup.exe"
