#!/usr/bin/env bash

set -e # tells the script to stop runing if error in execution, which it would not do normally

if [ -z "${AGENT_UPLOADER_USERNAME}" ]; then
    eval "$(fetch-agent-creds.sh)"
fi

DIR=`dirname ${0}`
cd ${DIR}

echo
echo
echo "============================================"
./src/agent/deploy.sh
echo
echo
echo "============================================"
./setup/windows/deploy.sh
