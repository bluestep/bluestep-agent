#!/usr/bin/env bash

DIR=`dirname ${0}`

set -e # tells the script to stop runing if error in execution, which it would not do normally


cd ${DIR}

[ ! -d ./node_modules ] && npm update && npm install

rm -r ../../dist/agent || true

echo "Begining Compilation - agent"
tsc
echo "Compilation Complete - agent"

cp ./package-lock.json ../../dist/agent
cp ./package.json ../../dist/agent
cp ./README.md ../../dist/agent
cp ./version.txt ../../dist/agent
cp -a ../../resources/hl7-test-files ../../dist/agent

