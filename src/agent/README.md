This is the primary project file. All common scripts (i.e. are platform independant) are to be stored in this file without additional organization, barring helper scripts which said common files may or may not share (which will be stored in the `subscripts` folder in this file).

Platform Specific scripts (such as a windows installer) are to be stored in their respective subfolders.

A dist.sh file for the platform is to be created in root. One day, a master dist.sh script will be written that will compile the respective platforms by merely passing a parameter. We could also get fancy with aliasing said bash script w/ parameters. Who knows.