#!/usr/bin/env bash

DIR=`dirname ${0}`

set -e # tells the script to stop runing if error in execution, which it would not do normally

cd ${DIR}

VERSION=${1}

if [ -z ${VERSION} ]; then
    echo "No version number provided"
    exit 1
fi

#if version doesn't match regular expression [0-9]+\.[0-9]+\.[0-9]+ then exit
if [[ ! "${VERSION}" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    echo "Version number must be in the format [0-9].[0-9].[0-9]"
    exit 1
fi

echo -n "${VERSION}" > ./version.txt

tmp=$(mktemp)
jq '.version = $version' --arg version "${VERSION}" ./package.json > "${tmp}" && mv "${tmp}" ./package.json

