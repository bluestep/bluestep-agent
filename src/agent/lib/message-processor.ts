import { Socket } from "net";
import { Buffer } from 'node:buffer';
import { SmartBuffer } from "smart-buffer";
import ack, { AckSettings } from "./ack.js";
import { AgentConfig } from "./agent-config.js";
import { dumpMllp } from "./log.js";
import { LoginConfig } from "./shared/login-config.js";


export interface MessageProccessorInput {
  agentConfig: AgentConfig;
  loginConfig: LoginConfig;
}

export interface MessageProcessor {
  process(socket: Socket, msgFn: (msg: string) => void): void;
}

export class MllpMessageProcessor implements MessageProcessor {
  encoding: BufferEncoding;
  requireEndOfData: boolean;
  startBuffer: Buffer;
  endBuffer: Buffer;
  loginConfig: LoginConfig;
  ack: AckSettings;
  debugLevel: number;
  dump: boolean;

  constructor(
    {
      agentConfig: {
        agent: {
          messageType: {
            requireEndOfData,
            encoding,
            bytes: {
              startByte,
              endByte,
            },
            ack,
            dump
          },
        },
        debugLevel
      },
      loginConfig
    }: MessageProccessorInput) {

    this.encoding = encoding;
    this.requireEndOfData = requireEndOfData;
    this.startBuffer = Buffer.from(startByte, "hex");
    this.endBuffer = Buffer.from(endByte, "hex");
    this.ack = ack;
    this.dump = dump;
    this.debugLevel = debugLevel;
    this.loginConfig = loginConfig;
  } // constructor

  process(socket: Socket, msgFn: (msg: string) => void): void {

    let outBuffer = SmartBuffer.fromOptions({ encoding: this.encoding });
    const startByte1 = this.startBuffer[0];
    const endByte1 = this.endBuffer[0]
    const endByte2 = this.endBuffer[1];

    let state = 0;

    socket.on('data', (chunk: Buffer) => {
      //console.log(`Received ${chunk.length} bytes of data.`);

      if (this.dump) {
        dumpMllp(chunk);
      }

      const inBuffer = SmartBuffer.fromBuffer(chunk, this.encoding);
      while (inBuffer.remaining() > 0) {
        let currentByte = inBuffer.readUInt8();
        //console.log(`state: ${state}, currentByte: ${currentByte}`);
        switch (state) {
          case 0: // start state
            if (!this.requireEndOfData) { // we shouldn't need to do messageDone() if there was an endbyte2
              this.messageDone(outBuffer, msgFn, socket);
            }
            outBuffer = SmartBuffer.fromOptions({ encoding: this.encoding });
            if (currentByte === startByte1) {
              // state = 0;
            } else if (currentByte === endByte1) {
              outBuffer.writeUInt8(currentByte);
              state = 2;
            } else {
              outBuffer.writeUInt8(currentByte);
              state = 1;
            }
            break;
          case 1: // got message character
            if (currentByte === startByte1) {
              state = 0;
            } else if (currentByte === endByte1) {
              outBuffer.writeUInt8(currentByte);
              state = 2;
            } else {
              outBuffer.writeUInt8(currentByte);
              // state = 1;
            }
            break;
          case 2: // got endbyte1
            if (currentByte === startByte1) {
              state = 0;
            } else if (currentByte === endByte1) {
              outBuffer.writeUInt8(currentByte);
              // state = 2;
            } else if (currentByte === endByte2) {
              this.messageDone(outBuffer, msgFn, socket);
              outBuffer = SmartBuffer.fromOptions({ encoding: this.encoding });
              state = 0;
            } else {
              outBuffer.writeUInt8(currentByte);
              state = 1;
            }
            break;
        }
      }

    });
  } // process  



  private messageDone(outBuffer: SmartBuffer, msgFn: (msg: string) => void, socket: Socket) {
    const hl7 = outBuffer.toString(); // convert the buffer to a string
    if (hl7) {
      msgFn(hl7);
      // console.log(hl7);
      if (this.ack) { //MLLP v1 does not require and ACK
        const ackMssg = ack(hl7, this.ack);
        if (ackMssg) {
          socket.write(
            Buffer.concat([
              this.startBuffer,
              Buffer.from(ackMssg, this.encoding),
              this.endBuffer
            ]));
        }
      }
    }
  }
}


const messageProcessors = new Map<string, MessageProcessor>();

export async function findMessageProcessor(input: MessageProccessorInput) {
  const messageType = input.agentConfig.agent.messageType.name;
  if (messageType === 'MLLP') {
    if (messageProcessors.has(messageType)) {
      return messageProcessors.get(messageType);
    } else {
      const mllpProcessor = new MllpMessageProcessor(input);
      messageProcessors.set(messageType, mllpProcessor);
      return mllpProcessor;
    }
  } else {
    throw new Error(`Unknown message type: ${messageType}`);
  }
}