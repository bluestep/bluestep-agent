import { readFile } from "fs/promises";

export const getRunningVersion = async () => {
  return (await readFile("./version.txt", "utf8")).trim();
}

export function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}