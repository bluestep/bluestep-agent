
export interface AckSettings {
  encoding?: string;  //default to "ASCII"
  version?: string;  //default to using sender's MSH-12
  timeFormat?: "julian" | "v24";  //default to "v24"
  excludeMilliseconds?: boolean; //default to false
  excludeTimezone?: boolean; //default to false
  dateTimePrecision?: "D" | "H" | "L" | "M" | "S" | "Y";  // Backwards compability only; D = Day, H = Hour, L = Month, M = Minute, S = Second, Y = Year
  overrideFnc?: string; //default to undefined
}

// Example MSH
//MSH|^~\&|SOFTWARE|Bluestep Test Pharmacy|BlueStep|TEST123|20200929093944||RDE^O11^RDE_O11|615889|P|2.5||||||ASCII|||
function parseMshSegment(hl7: string): string[] {
  const hl7Array = hl7.split(/[\r\n]+/);

  let mshSegment = hl7Array.find(seg => seg.includes("MSH"));
  if (!mshSegment) {
    console.error(`${new Date().toISOString()}: Ignoring message with no MSH segment:\n${hl7}`);
    return null;
  }
  const mshPosition = mshSegment.indexOf("MSH");
  mshSegment = mshSegment.substring(mshPosition);

  const separator = mshSegment.charAt(3);
  return mshSegment.split(separator);
}

function standardAck(hl7: string, ackSettings: AckSettings): string {
  return "julian" === ackSettings.timeFormat ? julianDateTimeAck(hl7, ackSettings) : v24DateTimeAck(hl7, ackSettings);
}

function julianDateTimeAck(hl7: string, ackSettings: AckSettings): string {
  return ack(hl7, ackSettings, () => Date.now().toString());
}

/**
 * Contains the exact time of an event, including the date and time. 
 * The date portion of a time stamp follows the rules of a date field and the time portion follows the rules of a time field. 
 * The time zone (+/-ZZZZ) is represented as +/-HHMM offset from UTC (formerly Greenwich Mean Time (GMT)), 
 * where +0000 or -0000 both represent UTC (without offset). 
 * The specific data representations used in the HL7 encoding rules are compatible with ISO 8824-1987(E).
 *
 * Format: YYYY[MM[DD[HHMM[SS[.S[S[S[S]]]]]]]][+/-ZZZZ]^<degree of precision>
 * 
 * Note: The time zone [+/-ZZZZ], when used, is restricted to legally-defined time zones and is represented in HHMM format.
 *
 * By site-specific agreement, YYYYMMDD[HHMM[SS[.S[S[S[S]]]]]][+/-ZZZZ]^<degree of precision> may be used where backward compatibility must be maintained.
 * 
 * First used in HL7 v2.4
 * @param ackSettings 
 * @returns
 */
function v24DateTimeAck(hl7: string, ackSettings: AckSettings): string {
  return ack(hl7, ackSettings, () => {
    const now = new Date();
    let rval = now.getFullYear() + ("0" + (now.getMonth() + 1)).slice(-2) + ("0" + now.getDate()).slice(-2)
      + ("0" + now.getHours()).slice(-2) + ("0" + now.getMinutes()).slice(-2) + ("0" + now.getSeconds()).slice(-2);
    if (!ackSettings.excludeMilliseconds) {
      rval += "." + ("00" + now.getMilliseconds()).slice(-3);
    }
    if (!ackSettings.excludeTimezone) {
      const timezoneOffset = now.getTimezoneOffset();
      const absTimezoneOffset = Math.abs(timezoneOffset);
      rval += (timezoneOffset < 0 ? "-" : "+") + ("0" + absTimezoneOffset / 60).slice(-2) + ("0" + absTimezoneOffset % 60).slice(-2);
    }
    if (ackSettings.dateTimePrecision) {
      rval += "^" + ackSettings.dateTimePrecision;
    }
    return rval;
  });
}

function ack(hl7: string, { version, encoding }: AckSettings, timeSegment: () => string): string {
  const mshSegment = parseMshSegment(hl7);
  if (!version) {
    version = mshSegment[11];
  }
  if (!encoding) {
    encoding = "ASCII";
  }
  return mshSegment
    ? `MSH|^~\\&|Bluestep|${mshSegment[5]}|${mshSegment[2]}|${mshSegment[3]}|${timeSegment()}||ACK|${mshSegment[9]}|P|${version}||||||${encoding}\rMSA|AA|${mshSegment[9]}\r`
    : null;
}

export default (hl7: string, ackSettings: AckSettings): string => {
  if (ackSettings) {
    if (ackSettings.overrideFnc) {
      /**
       * NOTE to future devs: eval is terrifying, and the fact that this works is the reason why.
       */
      const fn = eval(ackSettings.overrideFnc);
      return fn(hl7, ackSettings);
    } else {
      return standardAck(hl7, ackSettings);
    }
  } else {
    return null;
  }
};