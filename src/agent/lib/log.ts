import { appendFile, readdir, unlink, stat, existsSync, createWriteStream } from "fs";
import { mkdir, readFile, appendFile as appendFilePs } from "fs/promises";
import { sleep } from "./util.js";
import archiver from "archiver";
import { LoginConfig } from "./shared/login-config.js";
import path from "path";
import { Buffer } from 'node:buffer';
import { AuthFetch } from "./shared/net.js";

const logsDir = "../logs";

!existsSync(logsDir) && (await mkdir(logsDir));
/**
 * Agent's Logger. Creates logs based on the provided debugging tier. when you set a debugLevel in your configuration settings, you will get all logs who are at that number and up.
 *
 * 0: extremely granular debugging log (may cause issues if left on too long)
 *
 * 1: alerts on reception and sending
 *
 * 2: start/stop
 *
 * 3: only fatal errors
 *
 * 4+: stealth-mode (no logs)
 *
 * @param granularity the debug tier of this individual log. Depending on the debugLevel, the log will or not be recorded based on the value here
 * @param debugLevel provided by bluestep-agent.json; the user determines how granular the logs they want recorded
 * @param messageProducer simply a callback that produces the message contained to be written to the log
 */

let debugLevel: number = 0;

export const setDebugLevel = (value: number) => {
  debugLevel = value;
}

export const getDebugLevel = () => {
  return debugLevel;
}

export const log = (granularity: number, messageProducer: () => string) => {
  if (granularity <= debugLevel) {
    const todaysLogs = `${logsDir}/${formatDate()}.log`;
    updateStatLog(todaysLogs, `[${new Date().toISOString()}] ` + messageProducer());    
  }
};

const updateStatLog = async (name: string, data: string) => {
  await appendFilePs(name, "\n" + data + "\n");
};

export const deleteOldStatLogs = async () => {
  while (true) {
    console.log(`Deleting old logs from ${logsDir}`);
    readdir(logsDir, (err, files) => {
      if (err) {
        console.error(`${new Date().toISOString()}: Error reading - '${logsDir}':\n${err}`);
      }
      files.forEach(file => {
        stat(logsDir + "/" + file, (statErr, statStats) => {
          if (statErr) {
            console.error(`${new Date().toISOString()}: Cannot find file - '${file}':\n${statErr}`);
          }
          // potentially make the number of days kept a setting
          const weekAgoMs = Date.now() - 1000 * 60 * 60 * 24 * 7;
          if (statStats.birthtimeMs < weekAgoMs) {
            //console.log(`Deleting ${file}`);
            unlink(logsDir + "/" + file, fileErr => {
              if (fileErr) {
                console.error(`${new Date().toISOString()}: Unable to delete old file - '${file}':\n${fileErr}`);
              }
            });
          }
        });
      });
    });
    await sleep(24 * 60 * 60 * 1000); // 24 hours
  }
};

export const dumpMllp = (data: Buffer) => {
  const now = new Date();
  const fileName = `${logsDir}/${formatDateTime()}.mllp`;
  appendFile(fileName, data, err => {
    if (err) {
      console.error(`${new Date().toISOString()}: Unable to dump file:\n${fileName}`);
    }
  });
};

const tgzLogs = async () => {
  const zipFilename = `${formatDateTime()}.zip`;
  let found = [false, false, false];
  if (await dirLength(logsDir) > 0) {
    found[0] = true;
  }
  if (existsSync("../daemon/bluestep_agent.err.log")) {
    found[1] = true;
  }
  if (existsSync("../daemon/bluestep_agent.out.log")) {
    found[2] = true;
  }
  if (found.includes(true)) {

    const curDir = process.cwd();
    //get parent directory of curDir
    const parentDir = path.resolve(curDir, "..");
    const fullPath = path.resolve(parentDir, zipFilename);

    return new Promise<string>((resolve, reject) => {
      const output = createWriteStream(fullPath);
      const archive = archiver("zip");

      output.on("close", () => resolve(fullPath));
      output.on("error", err => reject(err));
      archive.on("error", (err: any) => reject(err));
      archive.pipe(output);

      found[0] && archive.directory("../logs", "logs");
      found[1] && archive.file("../daemon/bluestep_agent.err.log", { name: "daemon/bluestep_agent.err.log" });
      found[2] && archive.file("../daemon/bluestep_agent.out.log", { name: "daemon/bluestep_agent.out.log" });

      archive.finalize();
    });
  } else {
    return null;
  }
}

function dirLength(path: string): Promise<number> {
  return new Promise((resolve, reject) => {
    readdir(path, (err, files) => {
      if (err) {
        reject(err);
      }
      resolve(files.length);
    });
  });
}

export const uploadLogs = async (uploadLogsFetch: AuthFetch, { tpId, host }: LoginConfig, uploadLogInMinutes: number) => {
  if (uploadLogInMinutes > 0) {
    while (true) {
      const fullPath = await tgzLogs();
      if (fullPath) {
        try {
          console.log(`Uploading ${fullPath} to ${host}.`);
          // upload tgzFile to server using fetch
          const readFileData = await readFile(fullPath);
          const res = await uploadLogsFetch.fetch(`https://${host}/b/agent-logs?filename=${encodeURIComponent(path.basename(fullPath))}&tpId=${tpId}`,
            {
              method: "POST",
              body: readFileData,
              headers: {
                "Content-Type": "application/octet-stream"
              }
            });
          if (!res.ok) {
            console.error(`${new Date().toISOString()}: Error uploading logs: "${fullPath}" - ${res.status} - ${res.statusText}`);
          }
        } catch (err) {
          console.error(`${new Date().toISOString()}: Error proccessing "${fullPath}" - ${err}`);
        } finally {
          unlink(fullPath, unlinkErr => {
            if (unlinkErr) {
              console.error(`${new Date().toISOString()}: Error deleting "${fullPath}" - ${unlinkErr}`)
            };
          });
        }
      }
      await sleep(uploadLogInMinutes * 60 * 1000);
    }
  }
};

function formatDate(date?: number | string): string {
  if (!date) {
    date = Date.now();
  }

  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) {
    month = '0' + month;
  }
  if (day.length < 2) {
    day = '0' + day;
  }

  return [year, month, day].join('-');
}

function formatTime(time?: number | string) {
  if (!time) {
    time = Date.now();
  }

  var d = new Date(time),
    hour = '' + d.getHours(),
    minute = '' + d.getMinutes(),
    second = '' + d.getSeconds();

  if (hour.length < 2) {
    hour = '0' + hour;
  }
  if (minute.length < 2) {
    minute = '0' + minute;
  }
  if (second.length < 2) {
    second = '0' + second;
  }

  return [hour, minute, second].join('_') + '.' + ("00" + d.getMilliseconds()).slice(-3);

}

function formatDateTime(dateTime?: number | string) {
  return formatDate(dateTime) + '-' + formatTime(dateTime);
}