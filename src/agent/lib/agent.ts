import {
  existsSync, watch, writeFile
} from "fs";
import { mkdir, readdir, readFile, rename, rm, stat } from "fs/promises";
import { createServer, Socket } from "net";
import { AgentConfig, fetchAgentConfig } from "./agent-config.js";
import { deleteOldStatLogs, getDebugLevel, log, setDebugLevel, uploadLogs } from "./log.js";
import { findMessageProcessor } from "./message-processor.js";
import { LoginConfig, readLoginConfig } from "./shared/login-config.js";
import { AuthFetch } from "./shared/net.js";
import { getRunningVersion, sleep } from "./util.js";

// convenience-deconstructors
const { stringify } = JSON;
const { values } = Object;
// shared variables
let globalCounter: number = 0;

// function that returns a random string of length size (default 10)
const randomString = (size: number = 10): string => {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
  let result = '';
  for (let i = 0; i < size; i++) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return result;
};

const loginConfigs = await readLoginConfig("../");

interface SendDirArgs {
  dir: string;
  endpoint: string;
  authFetch: AuthFetch;
  queueDir: string;
  failqDir: string;
  tempDir: string;
  defaultDelay: number;
}

let processingQueue: { [key: string]: boolean } = {};
async function processSendQueue(sendDirArgs: SendDirArgs): Promise<void> {
  const sendDir = sendDirArgs.dir
  log(4, () => `Begin Processing Queue for dir: ${sendDir}`);
  if (processingQueue[sendDir]) {
    return;
  }
  processingQueue[sendDir] = true;
  do {
    await sendFilesInDir(sendDirArgs);
  } while ((await readdir(sendDirArgs.dir)).length > 0);
  processingQueue[sendDir] = false;
  log(4, () => `End Processing Queue for dir: ${sendDir}`);
}

/**
 * processes elements in the failq. Runs once every 5 hours
 */
async function processFailQueue(sendDirArgs: SendDirArgs): Promise<void> {
  while (true) {
    try {
      log(4, () => `Begin Processing Fail Queue`);
      // this is so it doesn't read the entire directory if we have no
      // intention of printing anything to the log
      if (getDebugLevel() > 1) {
        const beforeLength = (await readdir(sendDirArgs.failqDir)).length;
        log(1, () => `beforeLength: ${beforeLength}`);
      }
      sendDirArgs.dir = sendDirArgs.failqDir;
      await sendFilesInDir(sendDirArgs);
      if (getDebugLevel() > 1) {
        const afterLength = (await readdir(sendDirArgs.failqDir)).length;
        log(1, () => `afterLength: ${afterLength}`);
      }
      log(4, () => `End Processing Fail Queue`);
      await sleep(5 * 60 * 1000);
    } catch (e) {
      log(4, () => `Error processing fail queue:\n${e}`);
    }
  }
}

const sendFilesInDir = async (sendDirArgs: SendDirArgs): Promise<void> => {
  try {
    const files = await readdir(sendDirArgs.dir);
    for (let fileName of files) {
      try {
        await sendFile({...sendDirArgs, fileName});
      } catch (e) {
        log(1, () => `Error sending file: ${sendDirArgs.dir}/${fileName}\n${e}`);
      }
    };
  } catch (e) {
    log(1, () => `Error sending queue: ${sendDirArgs.dir}\n${e}`);
  }
}

interface SendFileArgs extends SendDirArgs {
  fileName: string;
}

const sendFile = async ({
  dir,
  fileName,
  endpoint,
  tempDir,
  authFetch,
  queueDir,
  failqDir,
  defaultDelay
}: SendFileArgs): Promise<void> => {

  const filePath = `${dir}/${fileName}`;
  log(4, () => `sendFile invoked for:\n${filePath}`);

  if (!fileName.endsWith(".message")) {
    log(1, () => `sendFile invoked for fileName that did not end in ".message". Deleting:\n${filePath}`);
    await rm(filePath);
    return;
  }

  const statsStats = await stat(filePath);
  if (statsStats.isFile()) {
    const tempFilePath = `${tempDir}/${fileName}`;

    log(4, () => `Attempting to rename ${filePath} to ${tempFilePath}`);
    await rename(filePath, tempFilePath);

    const readFileData = await readFile(tempFilePath);
    const url = endpoint + ("&counter=" + globalCounter++);
    log(3, () => `Reading ${tempFilePath} and sending on counter: ${globalCounter}`);
    try {
      for (let attempt = 0; attempt < 5; attempt++) {
        const response = await authFetch.fetch(url,
          {
            method: 'POST',
            headers: {
              'content-type': 'text/plain'
            },
            body: readFileData
          });
        log(3, () => `Received response for counter: ${url.split("counter=")[1]}`);
        const text = await response.text();
        log(4, () => `response text:\n${text}`);
        if (response.ok) {
          break;
        }
        log(4, () => `Failed attempt: ${attempt + 1}${attempt > 3 ? ", not retrying." : ", retrying in 10s."}`);
        if (attempt > 3 && !response.ok) {
          throw new Date().toISOString() + ": " + response;
        }
        await sleep(10_000);
      }

      try {
        log(4, () => `Attempting to remove from temp dir: ${tempFilePath}`);
        await rm(tempFilePath);
        log(4, () => `Removed from temp:\n${tempFilePath}`);
      } catch (e) {
        log(1, () => `Failed to remove from temp: ${tempFilePath}`);
      }
    } catch (e) {
      log(1, () => `Caught error making request to ${url}\n${e}`);
      if (dir === queueDir) {
        log(4, () => `Attempting to Rename to failq/:\n${filePath}`);
        await rename(tempFilePath, `${failqDir}/${fileName}`);
      }
    }
    await sleep(defaultDelay * 1000); // sleep for defaultDelay seconds.
  } else {
    log(1, () => `File is not a file: Deleting \n${filePath}`);
    await rm(filePath, { force: true, recursive: true, maxRetries: 10 });
  };
};


/**
 * Represents the arguments required for establishing a connection.
 */
interface ConnectionArgs {
  socket: Socket;
  agentConfig: AgentConfig;
  loginConfig: LoginConfig;
  queueDir: string;
  tempDir: string;
}

const onConnection = ({ socket, agentConfig, loginConfig, queueDir, tempDir }: ConnectionArgs): void => {
  log(3, () => `Connection detected and preparing to send message.`);
  findMessageProcessor({ agentConfig, loginConfig })
    .then(messageProcessor => messageProcessor.process(socket, msg => {
      log(4, () => `messageProcessor Triggered`);
      const fileName = `${randomString()}.message`;
      const path = `${tempDir}/${fileName}`;
      log(3, () => `Writing to path: ${path}`)
      writeFile(path, msg, writeFileErr => {
        if (writeFileErr) {
          throw new Date().toISOString() + ": " + writeFileErr;
        }
        rename(path, `${queueDir}/${fileName}`);
      });
    }));
};

const installedVersion = await getRunningVersion();

async function checkVersion(configChecksInMinutes: number, authFetch: AuthFetch, loginConfig: LoginConfig, agentConfig: AgentConfig) {
  while (true) {
    await sleep(configChecksInMinutes * 1000 * 60); // check every configChecksInMinutes for a new version
    try {
      const configCheck = await fetchAgentConfig(authFetch, loginConfig);
      if (!configCheck) {
        console.error(`${new Date().toISOString()}: Error checking for updates. Exiting ...`);
      } else if (JSON.stringify(configCheck) !== JSON.stringify(agentConfig)) {
        console.error(`${new Date().toISOString()}: New AgentConfig detected. Exiting ...`);
        process.exit(0);
      }
    } catch {
      console.error(`${new Date().toISOString()}: Error checking for updates. Exiting ...`);
      process.exit(15);
    }
  }
}

export default async () => {

  deleteOldStatLogs();

  let isFirst = true;
  for (const loginConfig of loginConfigs) {
    const { tpId, host } = loginConfig;

    const authFetch = new AuthFetch(loginConfig); // keepTrack of cookies

    const agentConfig = await fetchAgentConfig(authFetch, loginConfig);
    const {
      agent: {
        port,
        listenAddress,
      },
      defaultDelay,
      debugLevel,
      uploadLogInMinutes,
      configChecksInMinutes // default to 1 hour
    } = agentConfig; // destructure the configuration object

    const ipAddressAndPort = `${listenAddress}-${port}`;
    if (isFirst) {
      setDebugLevel(debugLevel);
      checkVersion(configChecksInMinutes, authFetch, loginConfig, agentConfig);
    }

    await checkQueueDirs(isFirst, ipAddressAndPort, "../queue");
    await checkQueueDirs(isFirst, ipAddressAndPort, "../failq");
    await checkQueueDirs(isFirst, ipAddressAndPort, "../temp");

    console.log(`${new Date().toISOString()}: Agent ${installedVersion} starting ...`);
    console.log(`${new Date().toISOString()}: Configured with \n${JSON.stringify(agentConfig, null, "  ")}`)
    log(2, () => `Agent Activated for ${ipAddressAndPort}`);
    uploadLogs(authFetch, loginConfig, uploadLogInMinutes);
    log(4, () => `Delete old stat logs protocol triggered.`);

    const endpoint = `https://${host}/b/bsMssg?tp=${encodeURIComponent(tpId)}`;
    const queueDir = `../queue/${ipAddressAndPort}`;
    const failqDir = `../failq/${ipAddressAndPort}`;
    const tempDir = `../temp/${ipAddressAndPort}`;
    const queueFiles = { dir: queueDir, endpoint, authFetch, queueDir, failqDir, tempDir, defaultDelay };
    processSendQueue(queueFiles);
    processFailQueue(queueFiles);  // failDir will be assigned to queueFiles.dir inside processFailQueue
    watch(queueDir, () => processSendQueue(queueFiles));
    log(4, () => `File watching is setup. for ${queueDir}`);
    const server = createServer();
    server.listen(port, listenAddress, () => log(2, () => `Server Listening on: ${listenAddress} : ${port}`));
    server.on('error', err => {
      log(1, () => `Server Error. Error thrown:\n${err}`);
      throw new Date().toISOString() + ": " + err;
    });
    server.on('connection', socket => onConnection({ socket, agentConfig, loginConfig, queueDir, tempDir }));
    isFirst = false;
  }


  /**
   * Checks the queue directories and moves any ".message" files to a specific directory.
   * 
   * @param isFirst - Indicates whether it is the first time checking the queue directories.
   * @param ipAddressAndPort - The IP address and port.
   * @param oldDir - The base directory to check for queue directories.
   */
  async function checkQueueDirs(isFirst: boolean, ipAddressAndPort: string, oldDir: string) {
    const newDir = `${oldDir}/${ipAddressAndPort}`;
    !existsSync(newDir) && (await mkdir(newDir, { recursive: true }));
    log(4, () => `${newDir} folder checked.`);

    if (isFirst) {
      const files = await readdir(oldDir);
      for (const file of files) {
        if (file.endsWith(".message")) {
          await rename(`${oldDir}/${file}`, `${newDir}/${file}`);
        }
      }
    }
  }
};
