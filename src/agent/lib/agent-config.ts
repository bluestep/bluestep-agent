import { existsSync } from "fs";
import { readFile, rename, writeFile } from "fs/promises";
import merge from "lodash.merge";
import { LoginConfig } from "./shared/login-config.js";
import { toJson, AuthFetch } from "./shared/net.js";
import { AckSettings } from "./ack.js";

export type MessageType = "MLLP"; // only MLLP is supported at this time

export interface Message {
  name: MessageType;
}

export interface MllpMessage extends Message {
  name: MessageType;
  bytes?: MllpMessageBytes;
  requireEndOfData?: boolean;
  encoding?: BufferEncoding;
  ack?: AckSettings;
  dump?: boolean;
}

export interface MllpMessageBytes {
  startByte?: string;
  endByte?: string;
}

export interface Agent {
  id: string;
  version: string;
  listenAddress?: string;
  port?: number;
  messageType: MllpMessage;
}

export interface AgentConfig {
  agent: Agent;
  debugLevel?: number;
  defaultDelay?: number;
  configChecksInMinutes?: number;
  uploadLogInMinutes?: number;
}

export async function fetchAgentConfig(agentConfigFetch: AuthFetch, { agentId, tpId, host }: LoginConfig): Promise<AgentConfig> {
  if (existsSync("../bluestep-agent.json")) {
    const data = await readFile("../bluestep-agent.json", "utf8");
    const url = `https://${host}/b/agent-config?agentId=${encodeURIComponent(agentId)}&tpId=${encodeURIComponent(tpId)}`;
    console.log(`Uploading bluestep-agent.json to ${url}`);
    const agentConfig = JSON.parse(data);
    const res = await agentConfigFetch.fetch(url, {
      method: "POST",
      body: JSON.stringify(agentConfig),
      headers: {
        "Content-Type": "application/json"
      }
    })
    if (!res.ok) {
      console.error(`Unable to upload bluestep-agent.json to BlueStep. Server returned status code: ${res.status}. ${res.statusText}`);
      throw new Error(`Unable to upload bluestep-agent.json to BlueStep. Server returned status code: ${res.status}. ${res.statusText}`);
    }
    const text = await res.text();
    if (text === 'success') {
      await rename("../bluestep-agent.json", "../bluestep-agent.json.old");
    } else {
      throw new Error('Unable to upload bluestep-agent.json to BlueStep. Server did not return "success".');
    }
    return await writeOfflineAgentConfig(agentConfig);
  } else {
    try {
      const config = <AgentConfig>await toJson(agentConfigFetch, `https://${host}/b/agent-config?agentId=${encodeURIComponent(agentId)}&tpId=${encodeURIComponent(tpId)}`);
      return await writeOfflineAgentConfig(config);
    } catch (e) {
      console.error("Unable to fetch agent config from BlueStep. Using local offline copy. Error: " + e.message);
      return applyDefaults((await readOfflineAgentConfig()));
    }
  }
}

async function readOfflineAgentConfig(): Promise<AgentConfig> {
  try {
    const data = await readFile("agent-config-offline.json", "utf8");
    return <AgentConfig>JSON.parse(data);
  } catch (e) {
    console.error("Unable to read offline agent config. Using defaults. Error: " + e.message)
    return <AgentConfig>{};
  }
}

async function writeOfflineAgentConfig(config: AgentConfig): Promise<AgentConfig> {
  if (!config) {
    throw new Error("Unable to write offline agent config. Agent Config is null or undefined.");
  }
  const parent = existsSync("./agent") ? "./" : "../";
  await writeFile(`${parent}/agent-config-offline.json`, JSON.stringify(config, null, 2));
  return applyDefaults(config);
}

function applyDefaults(config: AgentConfig): AgentConfig {
  // merge mutates the first argument and returns it
  return merge(
    {  //AgentConfig Defaults
      agent: {
        id: "",
        version: "0.0.0",
        port: 7003,
        listenAddress: '0.0.0.0',
        messageType: {
          name: 'MLLP',
          requireEndOfData: true,
          encoding: 'ascii',
          bytes: {
            startByte: "0b",
            endByte: "1c0d",
          },
          ack: {}
        },
      },
      defaultDelay: 10,
      debugLevel: 0,
      uploadLogInMinutes: 0,
      configChecksInMinutes: 60 // default to 1 hour      
    },
    config
  );
}

