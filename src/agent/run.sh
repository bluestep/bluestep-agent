#!/usr/bin/env bash

DIR=$(dirname ${0})

set -e # tells the script to stop runing if error in execution, which it would not do normally

if [ -z "${TEST_TP_USERNAME}" ]; then
    eval "$(fetch-agent-creds.sh)"
fi

cd ${DIR}

./dist.sh

mkdir -p ../../build

cp -a ../../dist/agent ../../build

pushd ../../build/agent >/dev/null
cp -a ../../src/agent/node_modules .

cat <<EOF >./login.json
{
  "username": "${TEST_TP_USERNAME}",
  "password": "${TEST_TP_PASSWORD}",
  "agentId": "dev-testing-d1638032-ee2c-45f6-a8a2-201db28e61b2",
  "tpId": "testPharm"
}
EOF

node index.js || true

rm -f ../login.json

popd >/dev/null

echo
echo "Delete ../../build/agent"
read -p "Delete? (y/n) " -n 1 -r
echo
if [[ ${REPLY} =~ ^[Yy]$ ]]; then
  rm -r ../../build/agent
fi
