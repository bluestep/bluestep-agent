
import fs from "fs";
import { readFile } from "fs/promises";
import { createConnection } from "net";
import { Buffer } from 'node:buffer';
import path from "path";
import { fileURLToPath } from 'url';
import { fetchAgentConfig } from "./lib/agent-config.js";
import { readLoginConfig } from "./lib/shared/login-config.js";
import { AuthFetch } from "./lib/shared/net.js";
import { on } from "events";


//esmodule doesn't support __dirname and __filename
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const loginConfigs = await readLoginConfig(`${__dirname}/../`);

const onlyPort = +process.argv[2];  //7000
let isOnlyPortFound = false;
for (const loginConfig of loginConfigs) {
  if (isOnlyPortFound) {
    break;
  }
  const authFetch = new AuthFetch(loginConfig); // keepTrack of cookies
  const agentConfig = await fetchAgentConfig(authFetch, loginConfig);

  // TODO create large suite of tests to send
  const {
    agent: {
      port, //7000
      messageType: { ack, bytes: { startByte, endByte }, encoding },
      listenAddress //0.0.0.0
    },
    defaultDelay
  } = agentConfig;

  if (onlyPort && port !== onlyPort) {
    console.log(`${new Date().toISOString()}: Skipping port ${port}`);
    continue;
  }
  if (onlyPort) {
    isOnlyPortFound = true;
  }

  console.log(`${new Date().toISOString()}: agentConfig:\n${JSON.stringify(agentConfig, null, 2)}`);
  console.log(`${new Date().toISOString()}: Using AgentConfig:\n${JSON.stringify(agentConfig, null, 2)}\n`);
  console.log(`${new Date().toISOString()}: Connecting to ${listenAddress}:${port}`);

  const clientSocket = createConnection(port, listenAddress);

  const startBuffer = Buffer.from(startByte, "hex");
  const endBuffer = Buffer.from(endByte, "hex");
  const startByte1 = startBuffer[0];
  const endByte1 = endBuffer[0];

  let ackString = "";
  let fileCount = 0;
  //This assumes the whole ack is in one chunk for testing purposes
  clientSocket.on("data", data => {
    console.log(`${new Date().toISOString()}: Received ${data.length} bytes of data.`);
    if (data.find(byte => byte === endByte1)) {
      //include bytes except startbyte and endbytes
      const ackString = data.slice(1, data.length - 2).toString(encoding).split("\r").join("\n");
      console.log(`\n${new Date().toISOString()}: Received message: \n${ackString}\n`);
      fileCount--;
      if (fileCount === 0) {
        clientSocket.end();
      }
    }
  });

  const addessAndPort = `${listenAddress}-${port}`;
  const failedFiles = await failqDirSize(addessAndPort);

  clientSocket.on("end", async () => {
    console.log(`${new Date().toISOString()}: End of connection.  Waiting ${defaultDelay} seconds to see if there are more files in failq.`);
    //sleep for 5 seconds to allow the server to process the files
    await new Promise(resolve => setTimeout(resolve, defaultDelay * 1000));
    const failedFiles2 = await failqDirSize(addessAndPort);
    console.log(`${new Date().toISOString()}: failedFiles=${failedFiles}, failedFiles2=${failedFiles2}`);
    if (failedFiles2 > failedFiles) {
      console.error(`\n${new Date().toISOString()}: Test failed. ${failedFiles2 - failedFiles} files were moved to the failq directory.\n`);
      process.exit(1);
    }
  });

  const directoryPath = path.join(__dirname, 'hl7-test-files');
  //passing directoryPath and callback function
  fs.readdir(directoryPath, async function (err, files) {
    //handling error
    if (err) {
      throw new Error('Unable to scan directory: ' + err);
    }
    //listing all files using forEach
    for (const file of files) {
      fileCount++;
      console.log(`\n${new Date().toISOString()}: Reading file: ${file}`);
      const data = await readFile(`${directoryPath}/${file}`);
      const message = data.toString();
      console.log(`\n${new Date().toISOString()}: Sending hl7 message for ${file}:\n${message}`);
      clientSocket.write(Buffer.concat([startBuffer, Buffer.from(message, encoding), endBuffer]));
      console.log(`\n${new Date().toISOString()}: Done sending hl7 message for ${file}\n`);
    };
    if (ack) {
      console.log(`${new Date().toISOString()}: Acks are expected for ${fileCount} messages.`);
    } else {
      console.log(`${new Date().toISOString()}: No ack expected. Ending connection.`);
      clientSocket.end();
    }
  });
}

function failqDirSize(addessAndPort: string): Promise<number> {
  return new Promise((resolve, reject) => {
    fs.readdir(path.join(__dirname, `../failq/${addessAndPort}`), (err, files) => {
      if (err) {
        reject(err);
      }
      resolve(files.length);
    });
  });
}