#!/usr/bin/env bash

DIR=$(dirname ${0})

set -e # tells the script to stop runing if error in execution, which it would not do normally

if [ -z "${AGENT_UPLOADER_USERNAME}" ]; then
    eval "$(fetch-agent-creds.sh)"
fi

cd ${DIR}

if [ -n "${1}" ]; then
  ./change-version.sh ${1}
fi

: "${AGENT_VERSION:=$(cat ./version.txt)}"

./dist.sh

pushd ../../dist >/dev/null
echo "Creating agent-${AGENT_VERSION}.tgz"
tar -zcvf agent-${AGENT_VERSION}.tgz agent
popd >/dev/null

echo "Uploading agent-${AGENT_VERSION}.tgz"
curl -X PUT -u "${AGENT_UPLOADER_USERNAME}:${AGENT_UPLOADER_PASSWORD}" \
  --upload-file "../../dist/agent-${AGENT_VERSION}.tgz" \
  "https://agent.bluestep.net/files/1475676/Agents/agent-${AGENT_VERSION}.tgz"
