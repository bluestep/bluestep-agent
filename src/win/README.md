## Bluestep-Agent

___
**Author:** Bluestep LLC <clientcare@bluestep.net> (https://www.bluestep.net)

**Desciption:** Bluestep-Agent will listen on a tcp/ip socket to transport messages and then send them to a server endpoint.

---
## Getting Started

0) Be sure to install node on your computer first. You may find a download link here:  https://nodejs.org/en/download/. If you don't know if you already have node, open a command line and use the command `node -v`. If you have node, this will display the version, if you don't, it will give an error.

TODO