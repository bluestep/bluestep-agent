#!/usr/bin/env bash

DIR=`dirname ${0}`

set -e # tells the script to stop runing if error in execution, which it would not do normally

cd ${DIR}

[ ! -d ./node_modules ] && npm update && npm install

rm -r ../../dist/win || true

echo "Begining Compilation - win-setup"
tsc
echo "Compilation Complete - win-setup"
