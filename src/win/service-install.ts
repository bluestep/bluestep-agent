import { LogMode, Service } from 'node-windows';

const loc = process.cwd().split("\\");
const script = loc.join("\\") + "\\index.js";
const svc = new Service({
  name: "BlueStep_Agent",  
  description: "For Sending data requests to BlueStep Servers",
  script
});

if (svc.exists) {
  console.log("Bluestep Agent Service already installed.  Stopping and reinstalling ...");
  svc.stop();
  svc.uninstall();
  svc.on('uninstall', () => {
    console.log('Uninstall complete.');
    console.log('The service exists: ', svc.exists);
    install();
  });
} else {
  install();
}

function install() {
  console.log(`Installing BlueStep Agent("${script}") as a Windows Service...`);
  svc.install();
  svc.on("install", () => {
    console.log("Starting BlueStep Agent service ...");
    svc.start();
  });
}
