#!/usr/bin/env bash

DIR=$(dirname ${0})

set -e # tells the script to stop runing if error in execution, which it would not do normally

trap 'finish' EXIT

finish() {
  if [ -f "${LOGIN_JSON}" ]; then
    echo "Removing ${LOGIN_JSON}"
    rm -f "${LOGIN_JSON}"
  fi
  if [ -d "${BUILD_DIR}/operator" ]; then
    echo
    echo "Delete ${BUILD_DIR}/operator"
    read -p "Delete? (y/n) " -n 1 -r
    echo
    if [[ ${REPLY} =~ ^[Yy]$ ]]; then
      rm -r "${BUILD_DIR}/operator"
    fi
  fi
}

if [ -z "${TEST_TP_USERNAME}" ]; then
    eval "$(fetch-agent-creds.sh)"
fi

cd ${DIR}

./dist.sh

mkdir -p ../../build
BUILD_DIR=$(readlink -f ../../build)
cp -a ../../dist/operator ../../build

pushd ../../build/operator >/dev/null
cp -a ../../src/operator/node_modules .

#set HOST varable to first argument if it exists or set to bst.bluestep.net
HOST=${1:-bst.bluestep.net}

cat <<EOF >./login.json
{
  "username": "${TEST_TP_USERNAME}",
  "password": "${TEST_TP_PASSWORD}",
  "agentId": "dev-testing-d1638032-ee2c-45f6-a8a2-201db28e61b2",
  "tpId": "testPharm",
  "host": "${HOST}"
}
EOF
LOGIN_JSON=$(readlink -f ./login.json)

node index.js || true

popd >/dev/null
