#!/usr/bin/env bash

DIR=`dirname ${0}`

set -e # tells the script to stop runing if error in execution, which it would not do normally

cd ${DIR}

[ ! -d ./node_modules ] && npm update && npm install

rm -r ../../dist/operator || true

echo "Begining Compilation - operator"
tsc
echo "Compilation Complete - operator"

cp ./package-lock.json ../../dist/operator
cp ./package.json ../../dist/operator
cp ./README.md ../../dist/operator
