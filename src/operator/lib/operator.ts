import { spawn, exec, fork } from "node:child_process";
import { checkVersionAndDownload } from "./downloader.js";
import * as util from "util";

// TODO: logarithmic backoff with a max retries and then exit and reset on successful run
export default async function() {
  const sleepIntervalStart = 1000; // 1 second
  const retries = 7;
  let retryCount = 0;
  while (true) {
    try {
      const startTime = Date.now();
      await startAgent();
      const endTime = Date.now();
      if (endTime - startTime < 1000) {
        throw new Error(`startAgent() took less than 1 second to complete.`);
      } else {
        retryCount = 0;
      }
    } catch (e) {
      console.log(e);
      ++retryCount;
      if (retryCount > retries) {
        console.log(`Operator: Too many retries: Exiting ...`);
        process.exit(15);
      } else {
        const sleepInterval = retryCount * retryCount * sleepIntervalStart;
        console.log(`Operator: ${retryCount}/${retries} - Sleeping for ${sleepInterval} milliseconds ...`);
        await util.promisify(setTimeout)(sleepInterval);
      }
    }
  }
 }


async function startAgent() {  
  const version = await checkVersionAndDownload();
  console.log(`Operator: Starting agent version ${version} ...`);
  const child = fork("./index.js", [], { cwd: process.cwd() + '/agent', stdio: 'inherit' });
  return new Promise<void>((resolve, reject) => {
    child.on('exit', (code) => {
      console.log(`Operator: Exited agent with code ${code}`);
      resolve();
    });
  });
}