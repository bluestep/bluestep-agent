import { exec } from "child_process";
import { readFile, rm } from "fs/promises";
import fetch from "node-fetch";
import { createWriteStream } from "node:fs";
import * as tar from "tar";
import * as util from "util";
import { readLoginConfig } from "./shared/login-config.js";
import { fetchVersion } from "./shared/net.js";

/**
 * Download a file to disk
 * @example downloadFile('https://agent.bluestep.net/public/Agent/agent.tgz', './agent.tgz')
 * @param {string} fileUrl - url of file to download
 * @param {string} destPath - destination path
 * @returns {Promise} resolves once complete, otherwise rejects
 */
const downloadFile = (fileUrl: string, destPath: string) => {

  if (!fileUrl) {
    return Promise.reject(new Error('Invalid fileUrl'));
  }
  if (!destPath) {
    return Promise.reject(new Error('Invalid destPath'));
  }

  return new Promise(function (resolve, reject) {

    fetch(fileUrl).then(function (res) {
      if (res.status !== 200) {
        return reject(new Error(`${fileUrl} - return status code: ${res.status}`));
      }
      var fileStream = createWriteStream(destPath);
      res.body.on('error', reject);
      fileStream.on('finish', resolve);
      res.body.pipe(fileStream);
    });
  });
}


const downloadAgent = async (version: string) => {
  //fetch agent code from server
  const url = `https://agent.bluestep.net/public/1475676/Agents/agent-${version}.tgz`;
  await downloadFile(url, './agent.tgz');
}


export const checkVersionAndDownload = async () => {
  const loginConfigs = await readLoginConfig();

  var agentInstalledVersion = 'Unknown';

  try {
    agentInstalledVersion = (await readFile("./agent/version.txt", 'utf8')).trim();
  } catch (e) {
    console.log("Operator: No agent installed");
  }

  interface VersionData {
    version: string;
  }

  //get agent version from server
  const version = await fetchVersion(loginConfigs[0]);

  if (!version) {
    throw Error("No version returned from server");
  }

  if (version !== agentInstalledVersion) {
    console.log(`Operator: Version mismatch - Current version ${agentInstalledVersion} != ${version}. Downloading new version of agent ...`);
    //delete agent folder if it exists
    await rm('./agent', { recursive: true, force: true });

    await downloadAgent(version);
    console.log(`Operator: Unzipping agent code to ${process.cwd()}/ ...`);

    await tar.x({ file: './agent.tgz' });

    await rm('./agent.tgz', { force: true });

    console.log("Operator: npm install");
    const execPromise = util.promisify(exec);
    const { stdout, stderr } = await execPromise(
      "npm install",
      { cwd: process.cwd() + '/agent' }
    );

    if (stderr) {
      throw new Error(stderr);
    }
    console.log(stdout);
  }
  return version;
}
