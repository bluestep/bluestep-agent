import { readFile } from "fs/promises";

const host = "bst.bluestep.net";

export interface UserCredentials {
    password: string;
    username: string;
    host: string;
}

export interface LoginConfig extends UserCredentials {
    agentId: string;
    tpId: string;
};

export const readLoginConfig = async (parent: string = "./") => {
    const objectOrArray = JSON.parse(await readFile(`${parent}login.json`, 'utf8')) as LoginConfig | LoginConfig[];
    const loginConfigs = Array.isArray(objectOrArray) ? objectOrArray : [objectOrArray];
    loginConfigs.forEach(loginConfig => loginConfig.host = loginConfig.host || host);    
    return loginConfigs;
};
