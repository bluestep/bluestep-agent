import fetch, { RequestInfo, Response, RequestInit } from "node-fetch";
import { Buffer } from 'node:buffer';
import { LoginConfig, UserCredentials } from "./login-config.js";

export const toJson = async (toJsonFetch: AuthFetch, url: string) => {
  const request = await toJsonFetch.fetch(url);
  if (!request.ok) {
    console.error(`${url} did not return status 200. Status was ${request.status} ${request.statusText}`);
    return null;
  }
  return await request.json();
}

interface VersionData {
  version: string;
}

export const fetchVersion = async (loginConf: LoginConfig) => {
  const authFetch = new AuthFetch(loginConf);
  const { tpId, agentId, host } = loginConf;
  const versionUrl = `https://${host}/b/agent-version?tpId=${encodeURIComponent(tpId)}&agentId=${encodeURIComponent(agentId)}`;
  const { version } = <VersionData>await toJson(authFetch, versionUrl);
  return version;
}

export class AuthFetch {
  private readonly host: string;
  private readonly auth: string;
  private requestCookies: object = {};

  constructor({host, username, password }: UserCredentials) {
    this.host = host;
    this.auth = `Basic ${Buffer.from(`${username}:${password}`).toString('base64')}`;
  }

  async fetch(url: RequestInfo, init?: RequestInit): Promise<Response> {
    if (!url.toString().includes(this.host)) { 
      throw new Error(`${url} must include ${this.host}`);
    }
    init = init || {};
    const headers = {
      Authorization: this.auth,
      Cookie: Object.entries(this.requestCookies).map(([key, value]) => `${key}=${value}`).join("; ")
    }
    init.headers = { ...headers, ...init.headers };

    const response = await fetch(url, init);
    const rawCookies = response.headers.raw()['set-cookie'];
    if (rawCookies) {
      const newCookies = rawCookies
        .map(cookieData => cookieData.split(/; ?/)[0]) // string
        .map(keyAndValue => keyAndValue.split('=')) // string[]
        .reduce(
          (m, [key, value]) => (m[key] = value, m), 
          <{ [key: string]: string; }>{}); // object
      this.requestCookies = { ...this.requestCookies, ...newCookies };
    }
    return response;
  }
}
