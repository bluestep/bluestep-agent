#!/usr/bin/env bash

cd $(dirname $0)

tsc agent_outbound.ts 
docker build . -t agent -t registry.ut.bluestep.dev:5000/agent
rm agent_outbound.js
docker push "registry.ut.bluestep.dev:5000/agent"

echo "restarting agent"
kubectl --namespace b6p-system rollout restart deployment agent

# or use this to update (or install) the deployment
eval "$(fetch-agent-creds.sh)"
if [[ -z $B6P_AGENT_TOKEN ]]; then
  echo "B6P_AGENT_TOKEN is not defined";
  exit 1
fi
helm upgrade --install --namespace b6p-system agent ./helm --set "secrets.B6P_AGENT_TOKEN=${B6P_AGENT_TOKEN}"