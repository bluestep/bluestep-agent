import * as express from "express";
import { createConnection } from "net"
import fetch from "node-fetch";

const app = express();
app.use(express.text());

app.post("/sendXML", (request, response) => {
  if (request.headers["x-token"] !== process.env.B6P_AGENT_TOKEN) {
    response.writeHead(403);
    response.end("Invalid Authentication");
  } else {
    const target_url = <string>request.headers["x-target-url"] || "localhost";


    console.log(`attempting to connect to: ${target_url}`);

    fetch(target_url, {
      method: "POST",
      headers: {
        "Content-Type": "application/xml"
      },
      body: request.body
    })
      .then(resp => resp.text())
      .then(ret => {
        console.log(ret)
        response.writeHead(200);
        response.end(ret);
      })
      .catch(e => {
        console.log("Error: " + e.message);
        e.stack && console.log("Stack: " + e.stack);
        response.writeHead(500);
        response.end(e.stack || e);
      });
  }
});
app.post("/sendhl7", (request, response) => {
  const text = request.body;

  if (request.headers["x-token"] !== process.env.B6P_AGENT_TOKEN) {
    response.writeHead(403);
    response.end("Invalid Authentication");
  } else {
    //console.log("recieve valid request")
    const start_byte = parseInt(<string>request.headers["start_byte"] || "11");
    const end_byte_1 = parseInt(<string>request.headers["end_byte_1"] || "28");
    const end_byte_2 = parseInt(<string>request.headers["end_byte_2"] || "13");
    const host = <string>request.headers["target_host"] || "localhost";
    const port = parseInt(<string>request.headers["target_port"] || "50010");

    console.log(`attempting to connect to: ${host}:${port}`);
    const socket = createConnection({ host, port }, () => {
      console.log(`connection achieved on: ${host}:${port}`);
      socket.write(new Uint8Array([start_byte]));
      socket.write(request.body);
      socket.write(new Uint8Array([end_byte_1, end_byte_2]));
      socket.on('data', resp => {
        response.writeHead(200);
        response.end(resp.toString());
        socket.end();
      });
    });

    socket.on('end', () => {
      console.log(`disconnected from: ${host}:${port}`)
    });
    socket.on('error', err => {
      console.log("Error: " + err.message);
      err.stack && console.log("Stack: " + err.stack);
      response.writeHead(500);
      response.end(err.message);
      socket.end();
    })

  }
});
app.get("/healthz", (request, response) => {
  response.writeHead(200);
  response.end();
});

app.listen(8080, "0.0.0.0", () => {
  console.log("Agent is Ready and listening on port 8080")
});




